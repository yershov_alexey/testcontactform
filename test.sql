-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 13 2014 г., 16:15
-- Версия сервера: 5.5.38-log
-- Версия PHP: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--
CREATE DATABASE IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `test`;

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fio` varchar(255) NOT NULL,
  `email` varchar(128) NOT NULL,
  `tel` varchar(14) NOT NULL,
  `message` text NOT NULL,
  `ip` varchar(15) NOT NULL,
  `dt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`id`, `fio`, `email`, `tel`, `message`, `ip`, `dt`) VALUES
(1, 'Ершов Алексей Федорович', 'yershov.alexey@gmail.com', '380501330872', 'skype: dancing_hamster', '127.0.0.1', '2014-10-13 12:11:53'),
(11, 'Ершов Алексей Федорович', 'lyershov@ukr.net', '', 'сообщение', '127.0.0.1', '2014-10-13 12:10:25'),
(8, 'Новый контакт', 'lyershov@ukr.net', '0501330872', 'sdfskf', '127.0.0.1', '2014-10-13 11:21:46'),
(10, 'Ершов Алексей Федорович', '', '12345', 'фывф', '127.0.0.1', '2014-10-13 12:07:53');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
