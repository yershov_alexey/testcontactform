<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset=utf-8">
	<title></title>
	<link rel="stylesheet" href="<?= $base_url ?>/res/css/admin_style.css" type="text/css" media="screen"/>
</head>

<body>

<div id="wrapper">

	<div id="header">
		<strong>Админка</strong>
	</div>
	<!-- #header-->

	<div id="middle">

		<div id="container">
			<div id="content">
				<?php
				if (is_array($inner_view)) {
					foreach ($inner_view as $item) {
						if ($item == 'message') {
							echo $message;
						} else
							$this->load->view($item . ".php");
					}
				} else {
					$this->load->view($inner_view . ".php");
				}
				?>

			</div>
			<!-- #content-->
		</div>
		<!-- #container-->

		<div class="sidebar" id="sideLeft">
			<a href="/">Фронтенд</a> <br/>
			<a href="/backend/logout">Выйти</a> <br/>
		</div>
		<!-- .sidebar#sideLeft -->

	</div>
	<!-- #middle-->

	<div id="footer">

	</div>
	<!-- #footer -->

</div>
<!-- #wrapper -->

</body>
</html>