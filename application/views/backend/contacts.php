<h2>Контакты</h2>
<?php if(isset($contacts) && !empty($contacts)): ?>
	<table>
		<th>id</th>
		<th>ФИО</th>
		<th>email</th>
		<th>телефон</th>
		<th>сообщение</th>
		<th>IP</th>
		<th>дата добавления</th>
		<th>удалить</th>
		<?php foreach($contacts as $item): ?>
			<tr>
				<td><?=$item->id;?></td>
				<td><?=$item->fio;?></td>
				<td><?=$item->email;?></td>
				<td><?=$item->tel;?></td>
				<td><?=$item->message;?></td>
				<td><?=$item->ip;?></td>
				<td><?=$item->dt;?></td>
				<td><a href="/backend/del/<?=$item->id;?>">X</a></td>
			</tr>
		<?php endforeach; ?>
	</table>
<?php endif; ?>
