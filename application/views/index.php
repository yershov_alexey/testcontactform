<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<link rel="stylesheet" href="<?=$base_url?>res/css/styles.css" type="text/css" media="screen"/>
	<title>Контактная форма</title>
</head>
<body>

<div id="container">
	<?php $this->load->view('header'); ?>
	<div id="body">
		<? if (isset($inner_view) && !empty($inner_view)): ?>
			<?php $this->load->view($inner_view); ?>
		<? endif; ?>
	</div>
	<?php $this->load->view('footer'); ?>

</div>

</body>
</html>