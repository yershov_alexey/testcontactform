<?php if(isset($success) && $success === false): ?>
	<span class="error_msg">Ошибка! Не удалось отправить данные. Попробуйте еще раз.</span>
<?php endif;?>
<h3>Пожалуйста, оставьте свои контактные данные</h3>
<span class="req"><?php echo validation_errors(); ?></span>
<span class="req">*</span> - поля обязательны к заполнению <br />
<span class="req_or">*</span> - должно быть заполнено одно из полей (укажите свой email или телефон)
<form method="POST" id="contact">
	<p class="contact"><span class="req">*</span> <label for="name">ФИО:</label></p>
	<input type="text" id="fio" name="fio" required value="<?=set_value('fio')?>"/><br />
	<p class="contact"><span class="req_or">*</span> <label for="email">E-mail:</label></p>
	<input type="email" id="email" name="email" value="<?=set_value('email')?>"/><br />
	<p class="contact"><span class="req_or">*</span> <label for="tel">Телефон:</label></p>
	<input type="tel" id="tel" name="tel" value="<?=set_value('tel')?>"/><br />
	<p class="contact"><label for="message">Сообщение:</label></p>
	<textarea id="message" name="message"><?=set_value('message')?></textarea><br />
	<input type="submit" id="send" name="send"/>
</form>
