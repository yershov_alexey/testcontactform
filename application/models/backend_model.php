<?php

class Backend_model extends CI_Model
{

	/**
	 * Проверка на наличие прав администратора
	 * @param string $login
	 * @param string $password
	 * @return bool
	 */
	public function isAdmin($login = null, $password = null)
	{
		$uhash = $this->session->userdata('uhash');
		if (isset($uhash) && !empty($uhash)) {
			if (md5($this->config->item('password_admin')) == $this->session->userdata('uhash')) {
				return true;
			}
		} elseif ($login == $this->config->item('login_admin') &&
			$password == md5($this->config->item('password_admin'))
		) {
			return true;
		}
		return false;
	}

	/**
	 * Получить список всех контактов
	 */
	public function getAllContacts()
	{
		return $this->db->query('SELECT * FROM contact')->result();
	}

	/**
	 * Удалить контакт
	 * @param int $id
	 */
	public function delContact($id = null)
	{
		if (!empty($id)) {
			$this->db->query("DELETE FROM contact WHERE id = ?", array($id));
		}
	}

}

?>