<?php

/**
 * Модель для тестового задания
 * Class Test_model
 */
class Test_model extends CI_Model
{

	/**
	 * Добавить контакт
	 * @param $article
	 */
	public function add_contact($contact)
	{
		$contact['ip'] = (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] :'');
		$this->db->query("INSERT INTO contact (fio, email, tel, message, ip, dt)
						  VALUES(?, ?, ?, ?, ?, NOW())",
			array(  $contact['fio'],
					$contact['email'],
					$contact['tel'],
					$contact['message'],
					$contact['ip'],
			));
		if ($this->db->insert_id() > 0) {
			// подготавливаем содержимое письма (текст можно вынести в lang файл для возможной мультиязычности )
			$message = "Добавлен новый контакт:
						ФИО: {$contact['fio']}
						email: {$contact['email']}
						Тел: {$contact['tel']}
						Сообщение: {$contact['message']}
						IP: {$contact['ip']}
						";
			// отправляем письмо
			$this->send_email($this->config->item('admin_email'), 'Новый контакт', $message);
			return true;
		}
		return false;
	}


	/**
	 * Отправка письма
	 *
	 * @param string $to
	 * @param string $subject
	 * @param string $message
	 */
	public function send_email($to, $subject, $message)
	{
		$this->load->library('email');
		$config['charset'] = 'utf-8';
		$this->email->initialize($config);
		$this->email->from($this->config->item('admin_email'), 'Site Name');
		$this->email->to($to);
		$this->email->subject($subject);

		$this->email->message($message);
		$this->email->send();
	}

}