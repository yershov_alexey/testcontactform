<?php

class Backend extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('backend_model', 'model');
		if (!$this->model->isAdmin() && $this->uri->segment(2) != 'login') {
			redirect('backend/login');
		}
	}

	public function index()
	{
		$this->data['contacts'] = $this->model->getAllContacts();
		$this->data['inner_view'] = "backend/contacts";
		$this->load->view('backend/main', $this->data);
	}

	public function login()
	{
		if ($this->input->post('enter')) {
			$name = trim($this->input->post('name'));
			$password = md5(trim($this->input->post('psw')));
			if ($this->model->isAdmin($name, $password)) {
				$this->session->set_userdata('uhash', $password);
			}
			redirect('backend/index');
		}
		$this->load->view('backend/login', $this->data);
	}

	public function logout()
	{
		$this->session->set_userdata('uhash', 0);
		redirect('backend/login');
	}

	/**
	 * Удалить контакт
	 * @param null $id
	 */
	public function del($id = null)
	{
		$this->model->delContact($id);
		redirect('backend/index');
	}

}

/* End of file backend.php */
/* Location: ./application/controllers/backend.php */