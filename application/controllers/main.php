<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('test_model', 'model');
	}

	public function index()
	{
		if ($this->input->post('send')) {
			$contact = array(
				'fio' => $this->input->post('fio'),
				'email' => $this->input->post('email'),
				'tel' => $this->input->post('tel'),
				'message' => $this->input->post('message'),
			);
			$this->form_validation->set_rules('fio', 'ФИО', 'trim|required');
			$this->form_validation->set_rules('email', 'email', "trim|valid_email|callback_req_tel_or_email[{$contact['tel']}]");
			$this->form_validation->set_rules('tel', 'Телефон', "trim|integer|min_length[5]|max_length[12]");
			$this->form_validation->set_rules('message', 'Сообщение', "trim");
			if ($this->form_validation->run() == true) {

				$this->data['success'] = false;
				if ($this->model->add_contact($contact)) {
					$this->data['success'] = true;
					redirect('/main/success');
				}
			}
		}
		$this->data['inner_view'] = 'main';
		$this->load->view('index', $this->data);
	}

	/**
	 * Данные успешно добавлены
	 */
	public function success()
	{
		$this->data['inner_view'] = 'success';
		$this->load->view('index', $this->data);
	}

	/**
	 * Функция валидации (Проверка того что указан мейл или телефон)
	 * @param $val1
	 * @param $val2
	 */
	function req_tel_or_email($val1, $val2)
	{
		if (empty($val1) && empty($val2)) {
			$this->form_validation->set_message('req_tel_or_email', 'Укажите email или телефон');
			return false;
		}
		return true;
	}


}

/* End of file main.php */
/* Location: ./application/controllers/main.php */